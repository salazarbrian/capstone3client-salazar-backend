const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = process.env.PORT || 4000;
const app = express();
require('dotenv').config();

mongoose.connect(process.env.DB_CONN, {useNewUrlParser: true, useUnifiedTopology: true,});
mongoose.connection.on('error', () => {console.log("Error connecting to database")})
mongoose.connection.once('open', () => {console.log("Connected to database")})

app.use(cors());
app.options('*', cors())
app.use(express.json());

const categoriesRoutes = require('./routes/categoryRoutes');
app.use('/api/categories', categoriesRoutes);

const ledgerRoutes = require('./routes/ledgerRoutes');
app.use('/api/ledger', ledgerRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/api/users', userRoutes);

app.listen(port, () =>{
	console.log(`App is listening on port ${port}`);
})
