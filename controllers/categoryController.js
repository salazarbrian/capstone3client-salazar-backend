const Categories = require('./../models/Category');


module.exports.addCategory = (req, res) => {
	Categories.find({name: req.body.name, user: req.decodedToken.id, type: req.body.type})
	.then(result => {
		if(result.length > 0){
			return res.send({msg: "Category Already Exist"})
		} 
			let category = {
			name: req.body.name,
			type: req.body.type,
			user: req.decodedToken.id
			}

			Categories.create(category)
				.then(result => res.send(result))
				.catch(err => res.send(err))
	})
}

module.exports.categoryDetails = (req, res) => {
	Categories.find()
		.then(result => res.send(result))
		.catch(err => res.send(err))
}

module.exports.UpdateTotalAmount = (req, res) => {

Categories.find({user: req.decodedToken.id})
		.then(result => res.send(result))
		.catch(err => res.send(err))
}



module.exports.addLedger = (req, res) => {

	Categories.findByIdAndUpdate({ _id: req.body._id}, 
	{$push : { transactions: { ledger: req.body.ledger}}
	},{new:true})
	.then(res.send(true))
	.catch(res.send(false))

}
