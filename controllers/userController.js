const User = require('./../models/User');
const Ledger = require('./../models/Ledger');
const bcrypt = require('bcrypt');
const auth = require('./../auth');
const {OAuth2Client} = require('google-auth-library')

//Add our clientID from OAuth Client Id. Make sure that the clientId used in the frontend is the same as the clientID provided here in the backend.
const clientId = "90581413699-ip64vs1bng77evmonv9gbeq064qu3n7h.apps.googleusercontent.com"

module.exports.emailExist = (req, res) => {
	User.findOne({email: req.body.email }).then(result => {
		return result.length > 0 ? true : false;
	})
}

module.exports.register = (req, res) => {

	let user = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo,
		loginType: "email"
	}

	return User.create(user)
			.then(res.send(true))
			.catch(res.send(false))
}

module.exports.login = (req, res) => {

	let { email, password } = req.body;
	console.log(req.body)
	User.findOne({email})
		.then( user => {

			if(user === null){ 
				return { 
					error: 'does-not-exist'
				}
			}

			let isPasswordMatched = bcrypt.compareSync(password,user.password);

			if (!isPasswordMatched){ 
			return { error: 'incorrect-password'} 
			}

			let accessToken = auth.createAccessToken(user)

			return  res.send({ accessToken : accessToken })

		})
}

module.exports.details = (req, res) => {

	User.findById(req.decodedToken.id, { password: 0 })
	.then( user => res.send(user))
	.catch( err => res.send(err))
}


module.exports.updateSavings = (req, res) => {

	User.findByIdAndUpdate(req.decodedToken.id,{
		$set: {savings : 0}
	})
	.then( () => {})

	User.findById(req.decodedToken.id)
	.then( result => {
		console.log(result)
		const ledger = result.transactions;
		const totalresult = 0;

		ledger.forEach( result => {
			Ledger.findById(result.ledger)
			.then( result => {
				let amount = 0

				if(result.type ===  'Income'){
					amount = result.amount
				}else{
					amount = -Math.abs(result.amount)
				}

				return User.findByIdAndUpdate(req.decodedToken.id,
					{
						$inc: { savings: amount }
					},
					{new: true}
					)
				})
		})
		return res.send(true)
	})


}	


		


module.exports.addLedger = (req, res) => {

	Categories.findByIdAndUpdate({ _id: req.decodedToken.id}, 
	{$push : { transactions: { ledger: req.body.ledger}}
	},{new:true})
	.then(res.send(true))
	.catch(res.send(false))

}

module.exports.verifyGoogleTokenId = async (tokenId) => {

	// console.log("From the User Controller:", tokenId)

	const client = new OAuth2Client(clientId);
	//Created a new client from OAuth2Client to be able to use methods for checking our google token id
	//With await, we will wait for the verifyIdToken function to resolve itself before proceeding. Whatever is returned by the verifyIdToken will be saved into the data variable.
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	});
	//Using our clientId and tokenId, we put them side by side to compare the clientId to the tokenId and verify if the tokenId is coming from a legitimate source.

	//Data will contain details from our user.
	console.log(data);

	//Added an if to check if the email is verified and our tokenId is true/legitimate:

	if (data.payload.email_verified === true){

		//Check if the google login user has already been registered in our app's db:
		const user = await User.findOne({ email: data.payload.email });

		//After checking if the user has registered, it will result into 2 outcomes:
		//user will contain USER DETAILS if the google login user has already registered in our app.
		//user will be null IF the google login user has not yet registered in our app.
		console.log(user);

		//Added an if statement to check if the user has already registered or not.
		//If the user has registered already the if statement will be run:
		if (user !== null) {

			//We will check the registered google login user if the email has been used to register through our registration page, if it is, then, there is a login-type-error and we will tell the user to login to through our regular login.

			if (user.loginType === "google") {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			} else {
				return { error: "login-type-error" }
			}

		} else {

			//If the user is null, therefore the user is not yet registered in our app's db.
			//We will create a new document for the new google login user:

			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: "google"
			})

			return newUser.save().then((user, err) => {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			})

		}

	} else {

		//We send this error if our tokenId is less than legitimate or if there were any errors checking our data:

		return { error: "google-auth-error" };

	}

}
