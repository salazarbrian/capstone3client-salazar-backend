const mongoose = require('mongoose');
const ledgerSchema = new mongoose.Schema({

	createdON: {
		type: Date,
		default: new Date()
	},
	dateOfTransaction: {
		type: Date,
		default: new Date()
	},
	amount:{
		type: Number,
		required: [true, "Amount is required."],
		default:0
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	name:{
		type: String,
		required: [true, "name is required."]
	},
	type: {
		type: String,
		required: [true, "Category Type is required."],
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User"
	},
	category: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Categories"
	}

})

module.exports = mongoose.model("Ledger", ledgerSchema);
