const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
	
	firstName:{
		type: String,
		required: [true, 'First Name is required.']
	},
	lastName:{
		type: String,
		required: [true, 'First Name is required.']
	},
	email: {
		type: String,
		required: [true, "Email is required"],
		unique: [true, "Email is already exists"]
	},
	savings: {
		type: Number
	},
	password: {
		type: String,
	},
	mobileNo: {
		type: String,
	},
	savings: {
		type: Number,
		required: [true, "savings is required"],
		default: 0
	},
	loginType: {
		type: String,
		required: [true, 'loginType is required.']
	},
	records: [
	{
		ledger:{
			type: mongoose.Schema.Types.ObjectId ,
			ref: "Ledger"
		}
	}]
})

module.exports = mongoose.model("User", userSchema);
