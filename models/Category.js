const mongoose = require('mongoose');
const categoriesSchema = new mongoose.Schema({

	name:{
		type: String,
		required: [true, 'Category Name is required.']
	},
	type: {
		type: String,
		required: [true, "Category Type is required."],
	},
	TotalAmount: {
		type: Number,
		required: [true, "Total Amount is required."],
		default: 0
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	user: {
		type: mongoose.Schema.Types.ObjectId ,
		ref: "User"
	},
	transactions: [{
		ledger:{
			type: mongoose.Schema.Types.ObjectId ,
			ref: "Ledger"
		}
	}]
		
})

module.exports = mongoose.model("Categories", categoriesSchema);
