const router = require('express').Router();
const UserController = require('./../controllers/userController');
const auth = require('./../auth');


router
	.route('/')
	.post(UserController.register)

router
	.route('/exist')
	.post(UserController.emailExist)

router
	.route('/details')
	.get(auth.verify, UserController.details)

router
	.route('/login')
	.post(UserController.login)

router
	.route('/savingsUpdate')
	.post(auth.verify, UserController.updateSavings)


//Currently working as intended :D <Add Transaction Id on User>; might not use in the future. might delete on the last stage of development
router
	.route(auth.verify, '/add')
	.post(UserController.addLedger)

router.post('/verify-google-id-token', async (req,res)=>{


	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))


})


module.exports = router;
