const router = require('express').Router();
const CategoryController = require('./../controllers/categoryController');
const auth = require('./../auth');

router
	.route('/')
	.post(auth.verify, CategoryController.addCategory)

router
	.route('/display')
	.get(CategoryController.categoryDetails)

router
	.route('/updateAmount')
	.get(auth.verify, CategoryController.UpdateTotalAmount)

router
	.route('/add')
	.post(CategoryController.addLedger)


module.exports = router;
