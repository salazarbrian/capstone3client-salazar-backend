const router = require('express').Router();
const LedgerController = require('./../controllers/ledgerController');
const auth = require('./../auth');

// router
// 	.route('/')
// 	.post(auth.verify , LedgerController.addTransaction)

router.post('/', auth.verify, LedgerController.addTransaction);

router
	.route('/display')
	.get(LedgerController.getAll)

module.exports = router;
